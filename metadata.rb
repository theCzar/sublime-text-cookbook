name             'sublime-text'
maintainer       'Patrick Ayoup'
maintainer_email 'patrick.ayoup@gmail.com'
license          'MIT'
description      'Installs/Configures Sublime Text'
long_description 'Installs/Configures Sublime Text'
supports 'ubuntu', ">= 14.04"

version          '0.1.1'

depends 'apt', '~> 2.4.0'
